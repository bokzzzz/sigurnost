<%@page import="DAO.ArticleDAO"%>
<%@page import="java.util.List" %>
<%@page import="sso.SSOCheck"%>
<%@page import="DTO.Article" %>
<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
    <%
    	request.setAttribute("page", "shop");
    	SSOCheck.check(request, response);
    %>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>Shop</title>
<style type="text/css">
input[type=text], input[type=number],select {
            width: 50%;
            padding: 4px 20px;
            margin: 10px 0;
            display: inline-block;
            border: 1px solid #ccc;
            box-sizing: border-box;
        }
 .button1 {
            background-color: #4CAF50;
            color: white;
            padding:5px 18px ;
            margin: 5px 5px;
            border: none;
            cursor: pointer;
            width: 30%;
            display: inline-block;
        }
 .buttonPlusMinus{
 margin: 10px 2px;
 pading:4px;
 }
        #table-wrapper {
       		max-height: 300px;
			width: 60%;
			padding:  15px;
			margin: 0px auto 0px auto;
			overflow: auto;
			float:left;
}
#table-buy {
       		max-height: 300px;
			width: 30%;
			padding:  15px;
			margin: 0px auto 0px auto;
			overflow: auto;
			float:right;
}
table {
	width: 100%;
  	max-height: 300px;
	text-align: left;
	border-collapse: collapse;
	background-color: #E9F9F9;
	position: relative;
	left:0px;
}


table > thead >tr >th  {
	background-color: #4CAF50;
	font-size: 20px;
	margin: 110%;
	padding: 10px;
	text-align: center;
	position: sticky;
	top:0;
	}
table > tbody > tr {
	font-size: 16px;
	color: #474747;
	background-color: #E9F9F9;
}
table > tbody > tr:hover {
	background-color: #DBF5F5;
}
}
table > tbody > tr > th {padding: 20px 25px;}
table > tbody > tr > th:nth-child(1),
table > tbody > tr > th:nth-child(2) {border-right: 1px solid white;}
table > tbody > tr > td {padding:2px 10px 2px 10px;text-align: center;}
}
</style>
</head>
<body>
<div id="table-wrapper">
      <table id="process-manager-table">
            <thead>
                	<tr>
                	<th>Name</th>
                	<th>Quantity</th>
               		<th>Price</th>
               		<th>Buy</th>
               	 	</tr>
           	</thead>
            <tbody>
                	<%
                		Article[] articles= new ArticleDAO().getArticles();
            			for(Article article :articles){
            				%>
            				
            					<tr id="<%= article.getId() %>">
            						
            						<td ><%= article.getName() %> </td>
            						<td><%= article.getQuantity() %></td>
            						<td><%= article.getPrice() %>&#8364  </td>
            						<td>
            							<form method="post" action="shop" class="form1" >            								
            								<input name="id"  type="hidden" value="<%= article.getId() %>" >
            								<input name="quantity"  type="number" min="1" max="<%= article.getQuantity() %>" >
            								<input  type="submit" class="button1"  name="buy" value="Buy">
            							</form>
            						</td>
            						
            					</tr>
            				
            				
            				<%
            			}
               	 %>
            	</tbody>
        	</table>
    	</div>
    	<div id="table-buy">
    		<table id="process-manager-table">
            <thead>
                	<tr>
                	<th>Name</th>
                	<th>Quantity</th>
               		<th>Price</th>
               	 	</tr>
           	</thead>
            	<tbody>
                	<%	
                	double totalPrice=0.0;
                	List<Article> articlesBuyed = (List)request.getSession().getAttribute("articles");
                	if(articlesBuyed != null){
            			for(Article article :articlesBuyed){
            				totalPrice+=article.getPrice();
            				%>
            					
            					<tr id="<%= article.getId() %>">
            						
            						<td ><%= article.getName() %> </td>
            						<td><%= article.getQuantity() %></td>
            						<td><%= article.getPrice() %>&#8364  </td>
            					</tr>
            				
            				
            				<%
            			}
                	}
               	 %>	
            	</tbody>
        	</table>
        	<p> <% if(totalPrice !=0) out.print("Total Price: "+ totalPrice +"&#8364");
        		%>  </p>
        	<p>
        	<%=
        	
        	request.getSession().getAttribute("result") != null ? request.getSession().getAttribute("result"):""
        	%>
        	</p>
        	<%
        		if(articlesBuyed != null && articlesBuyed.size() >0){
        			%>
        			<form method="get" action="shop">
        			<input  type="submit" class="button1"  name="confirm" value="Confirm">
        			</form>
        			<%
        		}
        	%>
        	
    	</div>
</body>
</html>