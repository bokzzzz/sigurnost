function changeButtonClick(username, role,button) {
	if(button.innerText == "Change")
	{
		console.log(button);
		$("#selRole").val(role);
		$("#username").val(username);  
		$("#buttonSubmit").val("Change User");
    
		button.innerText='Cancel';
		$("#username").prop("readonly", "readonly");
	}
	else {
		$("#selRole").val('admin');
		$("#username").val(""); 
		$("#username").removeAttr("readonly"); 
		$("#buttonSubmit").val("Add User");
		button.innerText='Change';
	}
}
function changeButtonArticle(id,button){
	if(button.innerText == "Change")
	{
		$("#buttonSubmit").val("Change Article");
		var elements=$("#"+id).children();
		console.log(elements);
		$("#name").val(elements[0].innerText);
		$("#quantity").val(elements[1].innerText);
		$("#price").val(elements[2].innerText);
		button.innerText="Cancel";
		$("#id").val(id);
	}
	else if(button.innerText == "Cancel"){
		$("#buttonSubmit").val("Add Article");
		$("#name").val("");
		$("#quantity").val("");
		$("#price").val("");
		button.innerText="Change";
	}
}