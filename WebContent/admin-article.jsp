<%@page import="sso.SSOCheck"%>
<%@page import="DTO.Article"%>
<%@page import="DAO.ArticleDAO"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
    <%
    	request.setAttribute("page", "admin-article");
    	SSOCheck.check(request, response);
    %>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script type="text/javascript" src="/admin.js"></script>
<title>Admin</title>
	<style>
        body {
            font-family: Arial, Helvetica, sans-serif;
            background-image: url('/background2.jpg')
        }

        form {
            border: 3px solid #f1f1f1;
            margin: 1% 30% 0 30%;
        }

        input[type=text], input[type=number],select {
            width: 100%;
            padding: 12px 20px;
            margin: 8px 0;
            display: inline-block;
            border: 1px solid #ccc;
            box-sizing: border-box;
        }
        .button {
            background-color: #4CAF50;
            color: white;
            padding: 14px 20px;
            margin: 8px 0;
            border: none;
            cursor: pointer;
            width: 100%;
        }
 .button1 {
            background-color: #4CAF50;
            color: white;
            padding:10px 18px ;
            margin: 0 0;
            border: none;
            cursor: pointer;
            width: 100%;
        }
        .form1{
        	background-color: #4CAF50;
            color: white;
            padding: 0;
            margin: 8px 0;
            border: none;
            cursor: pointer;
            width: 100%;
        }
        .button:hover {
            opacity: 0.8;
        }
        .container {
            padding: 16px;
        }
        #table-wrapper {
       		height: 300px;
			width: 60%;
			padding:  0;
			margin: 0px auto 0px auto;
			overflow: auto;
}

table {
	width: 100%;
  	height: 300px;
	text-align: left;
	border-collapse: collapse;
	background-color: #E9F9F9;
	position: relative;
}


table > thead >tr >th  {
	background-color: #4CAF50;
	font-size: 20px;
	margin: 110%;
	padding: 10px;
	text-align: center;
	position: sticky;
	top:0;
	}
table > tbody > tr {
	font-size: 16px;
	color: #474747;
	background-color: #E9F9F9;
}
table > tbody > tr:hover {
	background-color: #DBF5F5;
}
}
table > tbody > tr > th {padding: 20px 25px;}
table > tbody > tr > th:nth-child(1),
table > tbody > tr > th:nth-child(2) {border-right: 1px solid white;}
table > tbody > tr > td {padding:2px 10px 2px 10px;text-align: center;}
}
    </style>
</head>

<body>
    <form method="post" action="${pageContext.request.contextPath}/acarticle" style="background-color:white">
        <div class="container">
            <label for="name"><b>Name</b></label>
            <input id="id" maxlength="200" type="hidden"  name="id" value="">
            <input id="name" maxlength="200" type="text" placeholder="Enter Name" name="name" required>
            <label for="quantity"><b>Quantity</b></label>
            <input id="quantity" type="number" maxlength="200" name="quantity" placeholder="Enter Quantity"  min="0"  step="1" pattern="^[0-9]{7}$" required >
            <label for="price"><b>Price</b></label>
            <input id="price" type="number" maxlength="200" name="price" placeholder="Enter Price"  min="0"  step=".01" pattern="^[0-9]+\.[0-9]{1,2}$" required>
            <h3><%=session.getAttribute("result")!=null?session.getAttribute("result").toString():""%></h3>
            <input id="buttonSubmit" type="submit" class="button"  name="submit" value="Add Article">
        </div>
    </form>
    <hr />
    <div id="table-wrapper">
      <table id="process-manager-table">
            <thead>
                	<tr>
                	<th>Name</th>
                	<th>Quantity</th>
               		<th>Price</th>
               		<th>Change</th>
               		<th>Delete</th>
               	 	</tr>
           	</thead>
            <tbody>
                	<%
                		Article[] articles= new ArticleDAO().getArticles();
            			for(Article article :articles){
            				%>
            				
            					<tr id="<%= article.getId() %>">
            						
            						<td ><%= article.getName() %> </td>
            						<td><%= article.getQuantity() %></td>
            						<td><%= article.getPrice()%> </td>
            						<td><button class="button1" onclick="changeButtonArticle('<%= article.getId() %>',this)"> Change</button> </td>	
            						<td>
            							<form method="post" action="${pageContext.request.contextPath}/darticle?id=<%= article.getId() %>" class="form1" >
            								<input  type="submit" class="button1"  name="submit" value="delete">
            							</form>
            						</td>
            						
            					</tr>
            				
            				
            				<%
            			}
               	 %>
            	</tbody>
        	</table>
    	</div>
</body>
</html>