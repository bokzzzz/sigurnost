<%@page import="sso.SSOCheck"%>
<%@page import="DTO.User"%>
<%@page import="DAO.UserDAO"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
    <%
    	request.setAttribute("page", "admin");
    	SSOCheck.check(request, response);
    %>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script type="text/javascript" src="admin.js"></script>
<title>Admin</title>
	<style>
        body {
            font-family: Arial, Helvetica, sans-serif;
            background-image: url('background2.jpg')
        }

        form {
            border: 3px solid #f1f1f1;
            margin: 1% 30% 0 30%;
        }

        input[type=text], input[type=password],select {
            width: 100%;
            padding: 12px 20px;
            margin: 8px 0;
            display: inline-block;
            border: 1px solid #ccc;
            box-sizing: border-box;
        }
        .button {
            background-color: #4CAF50;
            color: white;
            padding: 14px 20px;
            margin: 8px 0;
            border: none;
            cursor: pointer;
            width: 100%;
        }
 .button1 {
            background-color: #4CAF50;
            color: white;
            padding:10px 18px ;
            margin: 0 0;
            border: none;
            cursor: pointer;
            width: 100%;
        }
        .form1{
        	background-color: #4CAF50;
            color: white;
            padding: 0;
            margin: 8px 0;
            border: none;
            cursor: pointer;
            width: 100%;
        }
        .button:hover {
            opacity: 0.8;
        }
        .container {
            padding: 16px;
        }
        #table-wrapper {
       		height: 300px;
			width: 60%;
			padding:  0;
			margin: 0px auto 0px auto;
			overflow: auto;
}

table {
	width: 100%;
  	height: 300px;
	text-align: left;
	border-collapse: collapse;
	background-color: #E9F9F9;
	position: relative;
}


table > thead >tr >th  {
	background-color: #4CAF50;
	font-size: 20px;
	margin: 110%;
	padding: 10px;
	text-align: center;
	position: sticky;
	top:0;
	}
table > tbody > tr {
	font-size: 16px;
	color: #474747;
	background-color: #E9F9F9;
}
table > tbody > tr:hover {
	background-color: #DBF5F5;
}
}
table > tbody > tr > th {padding: 20px 25px;}
table > tbody > tr > th:nth-child(1),
table > tbody > tr > th:nth-child(2) {border-right: 1px solid white;}
table > tbody > tr > td {padding:2px 10px 2px 10px;text-align: center;}
}
    </style>
</head>

<body>
    <form method="post" action="acuser" style="background-color:white">
        <div class="container">
            <label for="user"><b>Username</b></label>
            <input id="username" maxlength="200" type="text" placeholder="Enter Username" name="user" required>
            <label for="pass"><b>Password</b></label>
            <input type="password" maxlength="200" name="pass" placeholder="Enter Password" required>
            <label for="role"><b>Role</b></label>
            <select id="selRole" name="role" size="1" required>
                <option value="admin">Admin</option>
                <option value="admin-article">Admin Article</option>
                <option value="customer">Customer</option>
            </select>
            <h3><%=session.getAttribute("result")!=null?session.getAttribute("result").toString():""%></h3>
            <input id="buttonSubmit" type="submit" class="button"  name="submit" value="Add User">
        </div>
    </form>
    <hr />
    <div id="table-wrapper">
      <table id="process-manager-table">
            <thead>
                	<tr>
                	<th>Username</th>
                	<th>Role</th>
               		<th>Changer</th>
               		<th>Change</th>
               		<th>Delete</th>
               	 	</tr>
           	</thead>
            <tbody>
                	<%
                		User[] users= new UserDAO().getUsers();
            			for(User user :users){
            				%>
            				
            					<tr id="<%= user.getUsername() %>">
            						
            						<td ><%= user.getUsername() %> </td>
            						<td><%= user.getRole() %></td>
            						<td><%= user.getChanger() %> </td>
            						<td><button class="button1" onclick="changeButtonClick('<%= user.getUsername() %>','<%= user.getRole() %>',this)"> Change</button> </td>	
            						<td>
            							<form method="post" action="duser?username=<%= user.getUsername() %>" class="form1" >
            								<input  type="submit" class="button1"  name="submit" value="delete">
            							</form>
            						</td>
            						
            					</tr>
            				
            				
            				<%
            			}
               	 %>
            	</tbody>
        	</table>
    	</div>
</body>
</html>