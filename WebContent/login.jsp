<%@page import="jdk.nashorn.internal.runtime.regexp.joni.SearchAlgorithm.SLOW_IC"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
     <%@page import="attack.SlowHTTPAttack" %>
    <%
    	System.out.println("Adressa: " +request.getRemoteAddr());
    	if(SlowHTTPAttack.isBlocked(request.getRemoteAddr()))
    	{
    		System.out.println("Blokirana adressa");
    		response.setHeader("Connection", "close");
    		return;
    	}
    	if(request.getContentLength() < 5 || request.getContentLength()> 500)
    		SlowHTTPAttack.suspiciousPut(request.getRemoteAddr());
    	session.invalidate();
    	
    %>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Login security</title>
<style>
body {
	font-family: Arial, Helvetica, sans-serif;
	background-image: url('background2.jpg')
}
form {
	border: 3px solid #f1f1f1;
	margin: 5% 30% 0 30%;
}

input[type=text], input[type=password] {
  width: 100%;
  padding: 12px 20px;
  margin: 8px 0;
  display: inline-block;
  border: 1px solid #ccc;
  box-sizing: border-box;
}

.button {
  background-color: #4CAF50;
  color: white;
  padding: 14px 20px;
  margin: 8px 0;
  border: none;
  cursor: pointer;
  width: 100%;
}

button:hover {
  opacity: 0.8;
}


.imgcontainer {
  text-align: center;
  margin: 24px 0 12px 0;
}

img.avatar {
  width: 30%;
  border-radius: 40%;
}

.container {
  padding: 16px;
}

span.pass {
  float: right;
  padding-top: 16px;
}

/* Change styles for span and cancel button on extra small screens */
@media screen and (max-width: 300px) {
  span.psw {
     display: block;
     float: none;
  }
}
</style>
</head>
<body> 
	<form method="post" action="login" style="background-color:white">
		<div class="imgcontainer">
    		<img src="avatar.png" alt="Avatar" class="avatar">
     	</div>
     	<div class="container">
			<label for="user"><b>Username</b></label>
    		<input type="text" placeholder="Enter Username" name="user" required>
    		<label for="pass"><b>Password</b></label>
    		<input type="password" name="pass" placeholder="Enter Password"  required>
    		<input type="submit" class="button" value="Login">
    	</div>
</form>
</body>
</html>