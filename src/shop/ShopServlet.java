package shop;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import DAO.ArticleDAO;
import DAO.BillDAO;
import DTO.Article;
import DTO.Bill;
import attack.AttackCheck;
import attack.CookieAttack;
import attack.ParameterTamperingAttack;

/**
 * Servlet implementation class ShopServlet
 */
@WebServlet("/shop")
public class ShopServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ShopServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@SuppressWarnings("unchecked")
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String role = (String) request.getSession().getAttribute("role");
		if (role != null) {
			String submitValue = request.getParameter("confirm");
			String result = "";
			List<Article> articles = (List<Article>) request.getSession().getAttribute("articles");
			if (submitValue == null || submitValue.isEmpty() || articles == null || articles.isEmpty()
					|| !"Confirm".equals(submitValue) ||
					CookieAttack.isCookieAttack(request.getCookies(), request.getRemoteAddr())) {
				result = "Wrong Input!";
			} else {
				Bill bill = new Bill();
				bill.setArticles(articles.stream().toArray(Article[]::new));
				bill.setUsername((String) request.getSession().getAttribute("username"));
				result = new BillDAO().AddBill(bill);
				result = result.replace("\"", "");
				if ("True".equals(result)) {
					result = "Articles successfully bought!";
					articles.clear();
				} else
					result = "Order canceled!";
			}
			request.getSession().setAttribute("result", result);
			RequestDispatcher dispatcher = request.getRequestDispatcher("/shop.jsp");
			dispatcher.forward(request, response);
		} else
			response.sendRedirect("forbidden.html");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String role = (String) request.getSession().getAttribute("role");
		if (role != null) {
			String idString = request.getParameter("id");
			String quantityString = request.getParameter("quantity");
			String submitValue = request.getParameter("buy");
			String result = "";
			if (submitValue == null || submitValue.isEmpty() || !"Buy".equals(submitValue) || idString == null
					|| idString.isEmpty() || quantityString == null || quantityString.isEmpty() ||
					!AttackCheck.buyArticleCheck(idString,quantityString,submitValue, request.getRemoteAddr())
					|| CookieAttack.isCookieAttack(request.getCookies(), request.getRemoteAddr())) {
				result = "Wrong parameter";

			} else {
				try {
					int id = Integer.parseInt(idString);
					int quantity = Integer.parseInt(quantityString);
					Article article = new ArticleDAO().getArticleById(id);
					article.setPrice(article.getPrice() * quantity);
					article.setQuantity(quantity);
					AddArticleToList(request.getSession(), article);
					result = "Article " + article.getName() + " successfully added!";
				} catch (NumberFormatException e) {
					ParameterTamperingAttack.logParamTampAttack(request.getRemoteAddr());
					result = "Wrong parameter";
				}
			}
			request.getSession().setAttribute("result", result);
			RequestDispatcher dispatcher = request.getRequestDispatcher("/shop.jsp");
			dispatcher.forward(request, response);
		} else
			response.sendRedirect("forbidden.html");
	}

	@SuppressWarnings("unchecked")
	private List<Article> AddArticleToList(HttpSession session, Article article) {
		List<Article> articles = (List<Article>) session.getAttribute("articles");
		if (articles != null) {
			articles.add(article);
		} else {
			articles = new ArrayList<Article>();
			articles.add(article);
			session.setAttribute("articles", articles);
		}
		return articles;

	}

}
