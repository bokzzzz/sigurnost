package sso;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class SSOCheck {
	public static boolean check(HttpServletRequest request, HttpServletResponse response) throws IOException {
		String role = (String)request.getSession().getAttribute("role");
		String page=(String)request.getAttribute("page");
		if( role == null) {
			response.sendRedirect("login.jsp");
			return false;
		}
		if(role.equals("admin-article")) {
			if(page.equals("admin")) {
				response.sendRedirect("forbidden.html");
				return false;
			}
		}
		if(role.equals("customer") && !page.equals("shop")) {
			response.sendRedirect("forbidden.html");
			return false;
		}
		return true;
	}
}
