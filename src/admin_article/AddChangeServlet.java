package admin_article;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import DAO.ArticleDAO;
import DTO.Article;
import attack.AttackCheck;
import attack.CookieAttack;
import attack.ParameterTamperingAttack;

/**
 * Servlet implementation class AddChangeServlet
 */
@WebServlet("/acarticle")
public class AddChangeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public AddChangeServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String role = (String) request.getSession().getAttribute("role");
		if (role != null && (role.equals("admin") || role.equals("admin-article"))) {
			response.sendRedirect("/admin-article.jsp");
		} else
			response.sendRedirect("forbidden.html");

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String role = (String) request.getSession().getAttribute("role");
		if (role != null && (role.equals("admin") || role.equals("admin-article"))) {
			String action = (String) request.getParameter("submit");
			String result = "Wrong Input!";
			String name = request.getParameter("name");
			String priceString = request.getParameter("price");
			String quantityString = request.getParameter("quantity");
			if (name == null || name.isEmpty() || priceString == null || priceString.isEmpty() ||
					priceString ==  null || priceString.isEmpty() ||
					!AttackCheck.addOrChangeUserCheck(name, priceString, quantityString, role, action,
							request.getRemoteAddr()) ||
					CookieAttack.isCookieAttack(request.getCookies(), request.getRemoteAddr())) {
				result="Wrong Input!!";
			} else {
				try {
					int quantity = Integer.parseInt(quantityString);
					double price = Double.parseDouble(priceString);
					Article article = new Article(name, 0, quantity, price);

					if ("Add Article".equals(action)) {
						String resultFunc = new ArticleDAO().addArticle(article);
						resultFunc = resultFunc.replace("\"", "");
						if ("True".equals(resultFunc)) {
							result = "Article successfully added!";
						}

					} else if ("Change Article".equals(action)) {
						String idString = request.getParameter("id");
						int id = Integer.parseInt(idString);
						article.setId(id);
						String resultFunc = new ArticleDAO().changeArticle(article);
						resultFunc = resultFunc.replace("\"", "");

						if ("True".equals(resultFunc)) {
							result = "Article successfully changed!";
						}
					}

				} catch (NumberFormatException e) {
					ParameterTamperingAttack.logParamTampAttack(request.getRemoteAddr());
				}
			}
			request.getSession().setAttribute("result", result);
			RequestDispatcher dispatcher = request.getRequestDispatcher("/admin-article.jsp");
			dispatcher.forward(request, response);

		} else {
			response.sendRedirect("forbidden.html");
		}
	}

}
