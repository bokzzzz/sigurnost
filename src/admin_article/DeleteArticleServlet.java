package admin_article;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import DAO.ArticleDAO;
import attack.AttackCheck;
import attack.CookieAttack;
import attack.ParameterTamperingAttack;

/**
 * Servlet implementation class DeleteArticleServlet
 */
@WebServlet("/darticle")
public class DeleteArticleServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public DeleteArticleServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String role = (String) request.getSession().getAttribute("role");
		if (role != null && (role.equals("admin") || role.equals("admin-article"))) {
			String idString = request.getParameter("id");
			String result = "";
			if (idString == null || idString.isEmpty()
					|| !AttackCheck.deleteArticleCheck(idString, request.getRemoteAddr())
					|| CookieAttack.isCookieAttack(request.getCookies(), request.getRemoteAddr())) {
				result = "Wrong input!";
			} else {

				int id;
				try {
					id = Integer.parseInt(idString);
					result = new ArticleDAO().deleteArticle(id);
					result = result.replace("\"", "");
					if ("True".equals(result))
						result = "Article successfully deleted!";
					else
						result = "Wrong input!!!!!!!";

				} catch (NumberFormatException e) {
					ParameterTamperingAttack.logParamTampAttack(request.getRemoteAddr());
					result = "Wrong input!";
				}
			}
			request.getSession().setAttribute("result", result);
			RequestDispatcher dispatcher = request.getRequestDispatcher("/admin-article.jsp");
			dispatcher.forward(request, response);

		} else {
			response.sendRedirect("forbidden.html");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}

}
