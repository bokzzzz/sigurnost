package login;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.catalina.connector.Request;

import DAO.UserDAO;
import DTO.User;
import attack.AttackCheck;
import attack.CookieAttack;

/**
 * Servlet implementation class LoginServlet
 */
@WebServlet("/login")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LoginServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String username=request.getParameter("user");
		String password=request.getParameter("pass");
		if(AttackCheck.isSafeLogin(request, response) && !CookieAttack.isCookieAttack(request.getCookies(),request.getRemoteAddr())) {
		User user=new UserDAO().getLogin(new User(username,password));
		if(user != null) {
			request.getSession().setAttribute("role", user.getRole());
			request.getSession().setAttribute("username", username);
			if(user.getRole().equals("admin")){
				response.sendRedirect("admin.jsp");
			}
			else if(user.getRole().equals("admin-article")) {
				response.sendRedirect("admin-article.jsp");
			}
			else {
				response.sendRedirect("shop.jsp");
			}
		}
		else 
		response.sendRedirect("login.jsp");
		}else response.sendRedirect("login.jsp");
	}

}
