package DTO;

public class Bill {
	public Bill(Article[] articles, String username) {
		super();
		this.articles = articles;
		this.username = username;
	}
	private Article[] articles;
	public Article[] getArticles() {
		return articles;
	}
	public void setArticles(Article[] articles) {
		this.articles = articles;
	}
	public Bill() {
		super();
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	private String username;
}
