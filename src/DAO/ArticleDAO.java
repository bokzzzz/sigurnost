package DAO;

import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;

import DTO.Article;

public class ArticleDAO {
	static private WebTarget target=ClientBuilder.newClient().target("http://localhost:9000/security/article/");
	public Article[] getArticles() {
		return target.path("/articles")
                .request(MediaType.APPLICATION_JSON)
                .get(Article[].class);
	}
	public String addArticle(Article article) {
		return target.path("/add")
                .request(MediaType.APPLICATION_JSON)
                .post(Entity.entity(article,MediaType.APPLICATION_JSON),String.class);
	}
	public String changeArticle(Article article) {
		return target.path("/change")
                .request(MediaType.APPLICATION_JSON)
                .post(Entity.entity(article,MediaType.APPLICATION_JSON),String.class);
	}
	public String deleteArticle(int idArticle) {
		return target.path("/delete")
                .request(MediaType.APPLICATION_JSON)
                .post(Entity.entity(idArticle,MediaType.APPLICATION_JSON),String.class);
	}
	public Article getArticleById(int id) {
		return target.path("/id")
                .request(MediaType.APPLICATION_JSON)
                .post(Entity.entity(id,MediaType.APPLICATION_JSON),Article.class);
	}
}
