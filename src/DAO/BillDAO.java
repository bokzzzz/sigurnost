package DAO;

import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;

import DTO.Bill;

public class BillDAO {
	static private WebTarget target=ClientBuilder.newClient().target("http://localhost:9000/security/bill/");
	public String AddBill(Bill bill) {
		return target.path("/add")
                .request(MediaType.APPLICATION_JSON)
                .post(Entity.entity(bill,MediaType.APPLICATION_JSON),String.class);
	}
}
