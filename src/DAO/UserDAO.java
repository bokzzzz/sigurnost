package DAO;



import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;

import DTO.User;


public class UserDAO {
	static private WebTarget target=ClientBuilder.newClient().target("http://localhost:9000/security/");
		public User getLogin(User userLogin) {
			return target.path("/user")
                    .request(MediaType.APPLICATION_JSON)
                    .post(Entity.entity(userLogin,MediaType.APPLICATION_JSON),User.class);
			
		}
		public User[] getUsers() {
			return target.path("/users")
                    .request(MediaType.APPLICATION_JSON)
                    .get(User[].class);
		}
		public String addUser(User user) {
			return target.path("/add")
                    .request(MediaType.APPLICATION_JSON)
                    .post(Entity.entity(user,MediaType.APPLICATION_JSON),String.class);
		}
		public String changeUser(User user) {
			return target.path("/change")
                    .request(MediaType.APPLICATION_JSON)
                    .post(Entity.entity(user,MediaType.APPLICATION_JSON),String.class);
		}
		public String deleteUser(User user) {
			return target.path("/delete")
                    .request(MediaType.APPLICATION_JSON)
                    .post(Entity.entity(user,MediaType.APPLICATION_JSON),String.class);
		}
}
