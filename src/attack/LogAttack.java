package attack;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Date;

public class LogAttack {
	private static PrintWriter printWriter;
	static {
		try {
			printWriter=new PrintWriter("log.txt");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public static void log(String text,String attack,String address) {
		printWriter.append("=====================================================\n");
		printWriter.append("Attack: "+attack);
		printWriter.append("\nText attack: "+text);
		printWriter.append("\nAddress attacker: "+address);
		printWriter.append("\nTime: "+ (new Date()));
		printWriter.append("\n=====================================================");
		printWriter.flush();
	}
}
