package attack;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;




public class AttackCheck {
	public static boolean isSafeLogin(HttpServletRequest request, HttpServletResponse response) {
		String username=request.getParameter("user");
		String password=request.getParameter("pass");
		if(request.getContentLength() < 15 || request.getContentLength() > 500)
			SlowHTTPAttack.suspiciousPut(request.getRemoteAddr());
		return checkLoginAttack(username, password, request.getRemoteAddr());
	}
	private static boolean checkLoginAttack(String username,String password,String address) {
		MySqlInjection sqlIn=new MySqlInjection();
		boolean isSafe=true;
		isSafe=sqlIn.isSqlInjectionSafe(username);
		isSafe &=sqlIn.isSqlInjectionSafe(password);
		isSafe &= XSSAttack.isXSSSafe(username);
		isSafe &= XSSAttack.isXSSSafe(password);
		return isSafe;
	}
	public static boolean deleteAdminCheck(String username,String address) {
		boolean isSafe=true;
		MySqlInjection injection=new MySqlInjection();
		if(!injection.isSqlInjectionSafe(username)) {
			isSafe=false;
			LogAttack.log("SQLInjection-Delete User", username, address);
		}
		if(!XSSAttack.isXSSSafe(username)) {
			isSafe=false;
			LogAttack.log("XSS-Delete User", username, address);
		}		
		return isSafe;
	}
	public static boolean addOrChangeUserCheck(String username,String password,String changer, String role,String submit,String address) {
		boolean isSafe=true;
		MySqlInjection injection=new MySqlInjection();
		if(!injection.isSqlInjectionSafe(username)) {
			isSafe=false;
			LogAttack.log("SQLInjection-Add or Change User", username, address);
		}
		if(!injection.isSqlInjectionSafe(password)){
			isSafe=false;
			LogAttack.log("SQLInjection-Add or Change User", password, address);
		}
		if(!injection.isSqlInjectionSafe(changer)){
			isSafe=false;
			LogAttack.log("SQLInjection-Add or Change User", changer, address);
		}
		if(!injection.isSqlInjectionSafe(role)){
			isSafe=false;
			LogAttack.log("SQLInjection-Add or Change User", role, address);
		}
		if(!injection.isSqlInjectionSafe(submit)){
			isSafe=false;
			LogAttack.log("SQLInjection-Add or Change User", submit, address);
		}
		if(!XSSAttack.isXSSSafe(username) ) {
			isSafe=false;
			LogAttack.log("XSS-Add or Change User", username, address);
		}	
		if(!XSSAttack.isXSSSafe(password)) {
			isSafe=false;
			LogAttack.log("XSS-Add or Change User", password, address);
		}
		if(!XSSAttack.isXSSSafe(role)) {
			isSafe=false;
			LogAttack.log("XSS-Add or Change User", role, address);
		}
		if(!XSSAttack.isXSSSafe(submit)) {
			isSafe=false;
			LogAttack.log("XSS-Add or Change User", submit, address);
		}
		if(!XSSAttack.isXSSSafe(changer)) {
			isSafe=false;
			LogAttack.log("XSS-Add or Change User", changer, address);
		}
		return isSafe;
	}
	public static boolean addOrChangeArticleCheck(String name,String price,String quantity, String role,String submit,String address) {
		boolean isSafe=true;
		MySqlInjection injection=new MySqlInjection();
		if(!injection.isSqlInjectionSafe(name)) {
			isSafe=false;
			LogAttack.log("SQLInjection-Add or Change User", name, address);
		}
		if(!injection.isSqlInjectionSafe(price)){
			isSafe=false;
			LogAttack.log("SQLInjection-Add or Change User", price, address);
		}
		if(!injection.isSqlInjectionSafe(quantity)){
			isSafe=false;
			LogAttack.log("SQLInjection-Add or Change User", quantity, address);
		}
		if(!injection.isSqlInjectionSafe(role)){
			isSafe=false;
			LogAttack.log("SQLInjection-Add or Change User", role, address);
		}
		if(!injection.isSqlInjectionSafe(submit)){
			isSafe=false;
			LogAttack.log("SQLInjection-Add or Change User", submit, address);
		}
		if(!XSSAttack.isXSSSafe(name) ) {
			isSafe=false;
			LogAttack.log("XSS-Add or Change User", name, address);
		}	
		if(!XSSAttack.isXSSSafe(price)) {
			isSafe=false;
			LogAttack.log("XSS-Add or Change User", price, address);
		}
		if(!XSSAttack.isXSSSafe(role)) {
			isSafe=false;
			LogAttack.log("XSS-Add or Change User", role, address);
		}
		if(!XSSAttack.isXSSSafe(submit)) {
			isSafe=false;
			LogAttack.log("XSS-Add or Change User", submit, address);
		}
		if(!XSSAttack.isXSSSafe(quantity)) {
			isSafe=false;
			LogAttack.log("XSS-Add or Change User", quantity, address);
		}
		return isSafe;
	}
	public static boolean deleteArticleCheck(String id,String address) {
		boolean isSafe=true;
		MySqlInjection injection=new MySqlInjection();
		if(!injection.isSqlInjectionSafe(id)) {
			isSafe=false;
			LogAttack.log("SQLInjection-Delete User", id, address);
		}
		if(!XSSAttack.isXSSSafe(id)) {
			isSafe=false;
			LogAttack.log("XSS-Delete User", id, address);
		}		
		return isSafe;
	}
	public static boolean buyArticleCheck(String id,String quantity,String submit,String address) {
		boolean isSafe=true;
		MySqlInjection injection=new MySqlInjection();
		if(!injection.isSqlInjectionSafe(id)) {
			isSafe=false;
			LogAttack.log("SQLInjection-Add or Change User", id, address);
		}
		if(!injection.isSqlInjectionSafe(quantity)){
			isSafe=false;
			LogAttack.log("SQLInjection-Add or Change User", quantity, address);
		}
		if(!injection.isSqlInjectionSafe(submit)){
			isSafe=false;
			LogAttack.log("SQLInjection-Add or Change User", submit, address);
		}
		if(!XSSAttack.isXSSSafe(id) ) {
			isSafe=false;
			LogAttack.log("XSS-Add or Change User", id, address);
		}	
		if(!XSSAttack.isXSSSafe(quantity)) {
			isSafe=false;
			LogAttack.log("XSS-Add or Change User", quantity, address);
		}
		if(!XSSAttack.isXSSSafe(submit)) {
			isSafe=false;
			LogAttack.log("XSS-Add or Change User", submit, address);
		}
		return isSafe;
		
	}
}
