package attack;

import javax.servlet.http.Cookie;

public class CookieAttack {
	public static boolean isCookieAttack(Cookie[] cookies,String address) {
		boolean isSafe=true;
		for(Cookie cookie : cookies) {
		MySqlInjection injection=new MySqlInjection();
		if(!injection.isSqlInjectionSafe(cookie.getValue())) {
			isSafe=false;
			LogAttack.log("SQLInjection-Cookie", cookie.getValue(), address);
		}
		if(!XSSAttack.isXSSSafe(cookie.getValue()) ) {
			isSafe=false;
			LogAttack.log("XSS-Cookie", cookie.getValue(), address);
		}
		}
		return !isSafe;
	}
}
