package attack;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class SlowHTTPAttack {
	private static ConcurrentHashMap<String, Integer> ipAddress = new ConcurrentHashMap<String,Integer>();
	private static List<String> blockedAddress=new ArrayList<String>();
	
	static {
		blockedAddress.add("192.168.1.4");
		new Thread(() -> {
			while(true) {
				try {
					Thread.sleep(30000);
					System.out.println("Provjeravam napad");
					for (Map.Entry<String, Integer> entry : ipAddress.entrySet()) {
						if(entry.getValue() <= 5)
							ipAddress.remove(entry.getKey());
						else if(entry.getValue() > 20) {
							System.out.println("blokirao");
							if(!isBlocked(entry.getKey())) 
							{
								blockedAddress.add(entry.getKey());
							}
							ipAddress.remove(entry.getKey());
							LogAttack.log("Slow Http", "Slow Http attack", entry.getKey());
							
						}
						else
							ipAddress.put(entry.getKey(), 5);
					}
					
					   
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
		}).start();;
	}
	public static void suspiciousPut(String address) {
		int countAddress=ipAddress.getOrDefault(address, 0);
		ipAddress.put(address,countAddress+1);
	}
	public static boolean isBlocked(String address) {
		return blockedAddress.contains(address);
	}
	}

