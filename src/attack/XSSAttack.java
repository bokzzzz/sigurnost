package attack;

import java.util.regex.Pattern;

public class XSSAttack {
	public final static String tagStart = "\\<\\w+((\\s+\\w+(\\s*\\=\\s*(?:\".*?\"|'.*?'|[^'\"\\>\\s]+))?)+\\s*|\\s*)\\>";

    public final static String tagEnd = "\\</\\w+\\>";

    public final static String tagSelfClosing = "\\<\\w+((\\s+\\w+(\\s*\\=\\s*(?:\".*?\"|'.*?'|[^'\"\\>\\s]+))?)+\\s*|\\s*)/\\>";

    public final static String htmlEntity = "&[a-zA-Z][a-zA-Z0-9]+;";

    public final static Pattern htmlPattern = Pattern.compile("(" + tagStart + ".*" + tagEnd + ")|("+tagStart+".*"+tagStart+
    		")|(" + tagSelfClosing + ")|(" + htmlEntity + ")", Pattern.DOTALL);

	public static boolean isXSSSafe(String data) {
		boolean ret = true;
        if (data != null) {
            ret = !htmlPattern.matcher(data).find();
        }
        return ret;
	}

}
