package admin;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import DAO.UserDAO;
import DTO.User;
import attack.AttackCheck;
import attack.CookieAttack;

/**
 * Servlet implementation class AdminServlet
 */
@WebServlet("/acuser")
public class AddChangeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public AddChangeServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String role = (String) request.getSession().getAttribute("role");
		if (role != null && role.equals("admin")) {
			response.sendRedirect("admin.jsp");
		} else
			response.sendRedirect("forbidden.html");

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String roleUser = (String) request.getSession().getAttribute("role");
		if (roleUser != null && roleUser.equals("admin")) {
			String username = (String) request.getParameter("user");
			String password = (String) request.getParameter("pass");
			String role = (String) request.getParameter("role");
			String action = (String) request.getParameter("submit");
			String changer = (String) request.getSession().getAttribute("username");
			String result;
			if (username == null || username.isEmpty() || password == null || password.isEmpty() || role == null
					|| role.isEmpty() || action == null || action.isEmpty() || changer == null || changer.isEmpty()
					|| !AttackCheck.addOrChangeUserCheck(username, password, changer, role, action,
							request.getRemoteAddr()) || CookieAttack.isCookieAttack(request.getCookies(), request.getRemoteAddr())) {
				result = "Wrong Input!";
			} else {
				User user = new User();
				user.setUsername(username);
				user.setPassword(password);
				user.setChanger(changer);
				user.setRole(role);

				if ("Add User".equals(action)) {

					String isAdd = new UserDAO().addUser(user);

					isAdd = isAdd.replace("\"", "");
					if ("True".equals(isAdd))
						result = "User successfully added!";
					else
						result = "Wrong input!";

				} else if("Change User".equals(action)) {
					String isChange = new UserDAO().changeUser(user);
					isChange = isChange.replace("\"", "");
					if ("True".equals(isChange))
						result = "User successfully changed!";
					else
						result = "Wrong input!";
				}else result = "Wrong input!";

			}
			request.getSession().setAttribute("result", result);
			RequestDispatcher dispatcher = request.getRequestDispatcher("admin.jsp");
			dispatcher.forward(request, response);
		} else
			response.sendRedirect("forbidden.html");
	}

}
