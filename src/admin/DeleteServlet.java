package admin;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;



import DAO.UserDAO;
import DTO.User;
import attack.AttackCheck;
import attack.CookieAttack;
/**
 * Servlet implementation class DeleteServlet
 */
@WebServlet("/duser")
public class DeleteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DeleteServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String role=(String)request.getSession().getAttribute("role");
		if(role != null && role.equals("admin")) {
			String username=request.getParameter("username");
			String result="";
			if(username != null && !username.isEmpty() && AttackCheck.deleteAdminCheck(username, request.getRemoteAddr()
					) && !CookieAttack.isCookieAttack(request.getCookies(), request.getRemoteAddr())) {
				User user=new User();
				user.setUsername(username);
				result = new UserDAO().deleteUser(user);
				result=result.replace("\"", "");
				if("True".equals(result))
					result="User successfully deleted!";
				else result="Wrong input!";
				
			} else result="Wrong input!!";
			request.getSession().setAttribute("result", result );
			RequestDispatcher dispatcher = request.getRequestDispatcher("/admin.jsp");
			dispatcher.forward(request, response);
		}
		else {
			response.sendRedirect("forbidden.html");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		doGet(request,response);
	}

}
